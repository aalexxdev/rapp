import React, {useState}  from 'react';
import Toast from 'react-bootstrap/Toast';

import moment from 'moment';
// import Fade from 'react-bootstrap/Fade';
const Comment = props => {
    const [show, setShow] = useState(true);
    const toggleShow = () => setShow(!show);
    return (
<Toast show={show} onClose={toggleShow}>
    <Toast.Header>
      <strong className="mr-auto">{props.author}</strong>
        <small>{moment(props.created_at).fromNow()}</small>
    </Toast.Header>
        <Toast.Body>{props.text}</Toast.Body>
  </Toast>
    );
}


/*
    return (open&& (<Fade in={open}><Card style={{ width: '20rem',margin: '5px auto  5px' }}>
        <Card.Body>
            <Card.Title onClick={(e)=>{handleInfo(e, props.info.objectID)}}>{(props.info.title===null&& "- no title -") || props.info.title}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{props.info.author}</Card.Subtitle>
            <Card.Text>
                Created: {moment(props.info.created_at).format("MMM Do YY")}<br />
                Points: {props.info.points} <br />
                Comments: {props.info.num_comments || "-"}
            </Card.Text>
            {props.info.url!==null && <Card.Link target="_blank" href={props.info.url}> > link</Card.Link>}
           <FontAwesomeIcon style={{float: "right", color:"red", cursor:"pointer"}} onClick={() => setOpen(!open)} icon={faTrashAlt} />
        </Card.Body>
    </Card></Fade>))||null ;

};*/
export default Comment;